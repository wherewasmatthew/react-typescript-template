const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.resolve(__dirname, "../dist/js"),
    publicPath: path.resolve(__dirname, "assets"),
    filename: "main.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".mjs"],
    modules: ["node_modules"],
    fallback: { "crypto": require.resolve("crypto-browserify"), "stream": require.resolve("stream-browserify") }
  },
  devServer: {
    contentBase: path.join(__dirname, '../dist'),
    compress: true,
    historyApiFallback: true,
    publicPath: '/assets'
  },  
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        },
      },
      /*
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
      */
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(png|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[hash].[ext]",
              publicPath: "../logos/",
              outputPath: "../logos/",
            },
          }
        ],
      },
      /*
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader",
        exclude: /node_modules/
      }
      */
    ]
  },
  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between builds.
  externals: {
    //   react: "React",
    //   "react-dom": "ReactDOM"
  },
  plugins: [
    new CopyPlugin({
      patterns:[
        { from: './*.html', to: '../' }
      ]
    })
  ]
};