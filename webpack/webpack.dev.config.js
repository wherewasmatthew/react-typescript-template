const { merge } = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const common = require('./webpack.common.config');

module.exports = merge(common, {
  mode: "development",
  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",
  plugins: [
    // new Dotenv({
    //   path: './env/.env.dev'
    // })
  ]
});