# React Typescript Template

# Technologies
- <a href= "https://reactjs.org/">React Web</a> - Front-end WebApp interface library.
- <a href= "https://www.typescriptlang.org/">TypeScript</a> - Syntactical superset of JavaScript. Improves development process through tools such as strict type checking.
- <a href= "https://babeljs.io/">Babel</a> - Modern (ECMAScript 2015+) Javascript compiler.
- <a href= "https://webpack.js.org/">Webpack</a> - Javascript and web files bundler.
- <a href= "https://material-ui.com/">Material-UI</a> - Google styled out-of-the-box interface components, icons, themeing and typeface.
- <a href= "https://reactrouter.com/web/guides/quick-start">React-router-dom</a> - Page routing for react webapps.
- <a href="https://final-form.org/react">React Final Form</a> - High performance subscription-based form state management for React.
- <a href="https://github.com/lookfirst/mui-rff">MUI-RFF</a> - A thin wrapper that passes properties between MUI (Material-UI) and RFF (React Final Form) components.
